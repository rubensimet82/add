package com.nick80835.add

import android.content.SharedPreferences
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.net.URL
import java.text.DecimalFormat

class Downloader {
    private val twoDF = DecimalFormat("0.00")
    private var isCancelled = false
    private var currentProgress = 0
    lateinit var sharedPreferences: SharedPreferences
    lateinit var downloadContainer: DownloadContainer
    lateinit var service: AIDService

    fun runDownload() {
        service.newDownloadNotification(
            downloadContainer.taskId!!,
            downloadContainer.trackData!!.primaryName!!,
            downloadContainer.trackData!!.tertiaryName!!
        )

        try {
            val connection = URL(downloadContainer.downloadUrl!!).openConnection()
            connection.connect()

            val lengthOfFile = connection.contentLength

            val inputStream = connection.getInputStream()

            val outputFile = File(downloadContainer.filePathEncrypted!!)
            outputFile.createNewFile()

            var place = 0
            val chunk = ByteArray(16384)

            while (place < lengthOfFile) {
                val size = inputStream.read(chunk)
                val cutChunk = chunk.copyOfRange(0, size)

                place += size

                if (isCancelled) return
                updateProgress(place, lengthOfFile)
                outputFile.appendBytes(cutChunk)

                if (sharedPreferences.getBoolean("throttle_downloads", false)) {
                    Thread.sleep(50)
                }
            }

            inputStream.close()
        } catch (e: Exception) {
            Log.e(TAG, e.message!!)
            failDownload()
            return
        }

        Log.d(TAG, "Download succeeded: ${downloadContainer.taskId}")
        finishDownload()
    }

    private fun updateProgress(vararg values: Int?) {
        val progressPercent = (values[0]!! * 100) / values[1]!!

        if (progressPercent > currentProgress + 1 || progressPercent == 100) {
            currentProgress = progressPercent

            val progressSize = "${twoDF.format(values[0]!!.toFloat() / 1000000)}MB / ${twoDF.format(values[1]!!.toFloat() / 1000000)}MB"

            service.updateDownloadProgress(downloadContainer.taskId!!, currentProgress, progressSize)
        }
    }

    private fun finishDownload() {
        GlobalScope.launch {
            try {
                service.decryptDownloadedTrack(
                    downloadContainer.filePathEncrypted!!,
                    downloadContainer.blowfishKey!!,
                    downloadContainer.trackData!!,
                    downloadContainer.filePathComplete!!,
                    downloadContainer.taskId!!
                )
            } catch (e: Exception) {
                Log.e(TAG, e.message!!)
            }
        }
    }

    private fun failDownload() {
        Log.d(TAG, "Failing ${downloadContainer.taskId!!}")

        service.downloadEndNotification(
            downloadContainer.taskId!!,
            downloadContainer.trackData!!.primaryName!!,
            downloadContainer.trackData!!.tertiaryName!!,
            service.getString(R.string.download_failed)
        )

        File(downloadContainer.filePathEncrypted!!).delete()
        service.removeTask(downloadContainer.taskId!!)
        service.killIfNeeded()
    }

    fun cancelDownload() {
        isCancelled = true
        Log.d(TAG, "Cancelling ${downloadContainer.taskId!!}")
        service.downloadCancelNotification(downloadContainer.taskId!!)
        File(downloadContainer.filePathEncrypted!!).delete()
        service.removeTask(downloadContainer.taskId!!)
        service.killIfNeeded()
    }
}
