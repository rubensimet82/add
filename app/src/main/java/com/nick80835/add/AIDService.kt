package com.nick80835.add

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.*
import android.net.Uri
import android.os.*
import android.util.ArrayMap
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.preference.PreferenceManager
import ealvatag.audio.AudioFileIO
import ealvatag.tag.FieldKey
import ealvatag.tag.images.ArtworkFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.net.URL

const val serviceChannelID = "${BuildConfig.APPLICATION_ID}.service"
const val serviceNotificationID = 69
const val downloadChannelID = "${BuildConfig.APPLICATION_ID}.downloads"

var aidService: AIDService? = null

class AIDService : Service() {
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var decryptor: Decryptor
    private var activityMessenger: Messenger? = null
    private var serviceMessenger: Messenger = Messenger(IncomingHandler(this))
    private val backend = BackendFunctions()
    private var bound = false
    private var downloadTasks: ArrayMap<String, Downloader> = ArrayMap()

    override fun onCreate() {
        super.onCreate()
        aidService = this

        for (file in cacheDir.listFiles { file ->
            file.name.matches(Regex("(.*)encrypted"))
        }!!) {
            file.delete()
        }

        createNotificationChannel()
        startService()

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        decryptor = Decryptor().apply {
            sharedPreferences = this@AIDService.sharedPreferences
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(CancelReceiver(), IntentFilter())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "Service started")
        aidService = this
        activityMessenger = intent?.getParcelableExtra("Messenger")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d(TAG, "Service bound")
        aidService = this
        activityMessenger = intent?.getParcelableExtra("Messenger")
        bound = true
        return serviceMessenger.binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d(TAG, "Service unbound")
        activityMessenger = null

        return if (super.onUnbind(intent)) {
            bound = true
            true
        } else {
            bound = false
            killIfNeeded()
            true
        }
    }

    override fun onRebind(intent: Intent?) {
        Log.d(TAG, "Service rebound")
        aidService = this
        activityMessenger = intent?.getParcelableExtra("Messenger")
        bound = true
        super.onRebind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()

        for (task in downloadTasks) {
            cancelDownload(task.key)
        }

        for (file in cacheDir.listFiles { file ->
            file.name.matches(Regex("(.*)encrypted"))
        }!!) {
            file.delete()
        }
    }

    fun killIfNeeded() {
        if (downloadTasks.isEmpty() && !bound) {
            Log.d(TAG, "Killing service")
            stopSelf()
        }
    }

    private fun addTask(downloadTask: Downloader) {
        downloadTasks[downloadTask.downloadContainer.taskId!!] = downloadTask
    }

    fun removeTask(taskId: String) {
        downloadTasks.remove(taskId)
    }

    private fun startService() {
        val serverNotifIntent = Intent(applicationContext, MainActivity::class.java).apply {
            action = Intent.ACTION_MAIN
            addCategory(Intent.CATEGORY_LAUNCHER)
            addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }

        val serverPendingIntent = PendingIntent.getActivity(this, 0, serverNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val serverNotifBuilder = NotificationCompat.Builder(this, serviceChannelID).apply {
            setContentTitle(resources.getString(R.string.app_name))
            setTicker(resources.getString(R.string.app_name))
            setContentText(resources.getString(R.string.service_running))
            setSmallIcon(R.drawable.ic_ongoing_notif)
            setContentIntent(serverPendingIntent)
            setCategory(NotificationCompat.CATEGORY_SERVICE)
            setVisibility(NotificationCompat.VISIBILITY_SECRET)
            priority = NotificationCompat.PRIORITY_MIN
        }

        startForeground(serviceNotificationID, serverNotifBuilder.build())
    }

    private class IncomingHandler(val aidService: AIDService) : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.data.get("task") as String) {
                "request_download" -> {
                    val thisData = msg.obj as TrackData
                    val quality = msg.data.get("quality") as Int
                    val albumDownload = msg.data.get("albumDownload") as Boolean

                    GlobalScope.launch {
                        try {
                            aidService.downloadTrack(thisData, quality, albumDownload)
                        } catch (e: Exception) {
                            Log.e(TAG, e.message!!)
                        }
                    }
                }
            }
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val thisNotificationManager = getSystemService(NotificationManager::class.java)!!

            val servicename = "Service Notification"
            val servicedescription = "The notification present when AIDS is running."
            val servicechannel = NotificationChannel(serviceChannelID, servicename, NotificationManager.IMPORTANCE_MIN)
            servicechannel.description = servicedescription
            servicechannel.setSound(null, null)
            thisNotificationManager.createNotificationChannel(servicechannel)

            val downloadname = "Download Notifications"
            val downloaddescription = "Notifications providing the status of downloads."
            val downloadchannel = NotificationChannel(downloadChannelID, downloadname, NotificationManager.IMPORTANCE_DEFAULT)
            downloadchannel.description = downloaddescription
            downloadchannel.setSound(null, null)
            thisNotificationManager.createNotificationChannel(downloadchannel)
        }
    }

    private fun generateFriendlyName(thisData: TrackData, album: Boolean = false): String {
        val template = if (album) {
            sharedPreferences.getString("album_trackname_template", "number - title")
        } else {
            sharedPreferences.getString("trackname_template", "artist - title")
        }

        val numberPadding = sharedPreferences.getInt("track_number_padding", 0) + 1

        return template!!.replace("artist", thisData.tertiaryName!!)
            .replace("title", thisData.primaryName!!)
            .replace("album", thisData.secondaryName!!)
            .replace("number", thisData.tagData!!.trackNumber?.toString()?.padStart(numberPadding, '0') ?: "")
            .replace("contributors", thisData.tagData!!.trackContributors ?: "")
            .replace("date", thisData.tagData!!.trackDate ?: "")
            .replace("genre", thisData.tagData!!.trackGenre ?: "")
            .replace("trackid", thisData.trackId!!.toString())
    }

    private fun downloadTrack(thisData: TrackData, quality: Int, albumDownload: Boolean = false) {
        val thisTaskId = "${thisData.trackId}_$quality"
        val extension = if (quality == 9) {"flac"} else {"mp3"}
        var downloadPath = sharedPreferences.getString("download_path", "${Environment.getExternalStorageDirectory().absolutePath}/Music/Deezer")!!

        val artistSep = when (sharedPreferences.getInt("artist_separator", 0)) {
            0 -> ", "
            1 -> ","
            2 -> "; "
            3 -> ";"
            4 -> " / "
            5 -> "/"
            6 -> " & "
            7 -> "&"
            8 -> "\uE000"
            else -> ", "
        }

        thisData.tagData = backend.getTrackTagData(thisData, artistSep)
        var friendlySaveName = generateFriendlyName(thisData)

        if (sharedPreferences.getBoolean("create_album_folders", true) && albumDownload) {
            downloadPath += "/${thisData.secondaryName!!.scrub()} - ${thisData.tertiaryName!!.scrub()}"
            friendlySaveName = generateFriendlyName(thisData, true)
        }

        File(downloadPath).mkdirs()

        val thisFilePathEncrypted = "$cacheDir/$thisTaskId.$extension.encrypted"
        val thisFilePathComplete = "$downloadPath/${friendlySaveName.scrub()}.$extension"

        if (File(thisFilePathComplete).exists()) {
            requestSnackbar(getString(R.string.already_downloaded, thisData.primaryName))
            return
        }

        val thisSID = decryptor.getSID()
        val thisApiToken = decryptor.getApiToken(thisSID)

        if (((thisApiToken["USER"] as Map<*, *>)["USER_ID"] as Double) == 0.toDouble()) {
            requestSnackbar("Invalid ARL detected, resetting ARL")

            try {
                activityMessenger?.send(Message().apply {
                    obj = "request_default_tonke"
                })
            } catch (e: Exception) {
                Log.w(TAG, e.message!!)
            }

            return
        }

        if (sharedPreferences.getBoolean("get_synced_lyrics", false) || sharedPreferences.getBoolean("get_embedded_lyrics", false)) {
            val thisTrackLyrics = backend.getTrackLyrics(thisData.trackId!!, thisSID)

            if (thisTrackLyrics.first != null && sharedPreferences.getBoolean("get_synced_lyrics", false)) {
                val lyricsFile = File("$downloadPath/${friendlySaveName.scrub()}.lrc")

                if (!lyricsFile.exists()) {
                    lyricsFile.createNewFile()
                    lyricsFile.appendText(thisTrackLyrics.first!!)
                }
            }

            if (thisTrackLyrics.second != null && sharedPreferences.getBoolean("get_embedded_lyrics", false)) {
                thisData.tagData!!.trackLyrics = thisTrackLyrics.second
            }
        }

        val thisBlowfishKey = decryptor.createBlowfishKey(thisData.trackId!!)
        val thisTrackSecretData = decryptor.getTrackSecrets(thisData.trackId!!, thisApiToken["checkForm"] as String, thisSID)!!

        if (quality == 1 && thisTrackSecretData["FILESIZE_MP3_128"] == "0") {
            requestSnackbar(getString(R.string.quality_not_available, thisData.primaryName, "MP3 128"))
            return
        } else if (quality == 3 && thisTrackSecretData["FILESIZE_MP3_320"] == "0") {
            requestSnackbar(getString(R.string.quality_not_available, thisData.primaryName, "MP3 320"))
            return
        } else if (quality == 9 && thisTrackSecretData["FILESIZE_FLAC"] == "0") {
            requestSnackbar(getString(R.string.quality_not_available, thisData.primaryName, "FLAC"))
            return
        }

        val thisPUID: String

        try {
            thisPUID = thisTrackSecretData["MD5_ORIGIN"] as String
        } catch (e: Exception) {
            requestSnackbar("Failed to get download link for ${thisData.primaryName}")
            return
        }

        val thisMediaVersion = thisTrackSecretData["MEDIA_VERSION"] as String

        val downloadUrl = decryptor.getDownloadUrl(thisData.trackId!!, thisPUID, thisMediaVersion, quality)

        val thisDownloadTask = Downloader()

        thisDownloadTask.sharedPreferences = sharedPreferences
        thisDownloadTask.service = this

        thisDownloadTask.downloadContainer = DownloadContainer().apply {
            this.blowfishKey = thisBlowfishKey
            this.filePathComplete = thisFilePathComplete
            this.filePathEncrypted = thisFilePathEncrypted
            this.trackData = thisData
            this.taskId = thisTaskId
            this.downloadUrl = downloadUrl
        }

        addTask(thisDownloadTask)

        GlobalScope.launch {
            try {
                thisDownloadTask.runDownload()
            } catch (e: Exception) {
                Log.e(TAG, e.message!!)
            }
        }
    }

    fun decryptDownloadedTrack(
        thisFilePathEncrypted: String,
        thisBlowfishKey: String,
        thisData: TrackData,
        thisFilePathComplete: String,
        downloadTaskId: String) {

        var chunkCounter = 0

        val thisEncryptedFile = File(thisFilePathEncrypted)
        val thisDecryptedFile = File(thisFilePathComplete)
        thisDecryptedFile.createNewFile()

        thisEncryptedFile.forEachBlock(2048) { chunk, bytesRead ->
            val decChunk: ByteArray = if ((chunkCounter % 3) > 0 || bytesRead < 2048) {
                chunk
            } else {
                decryptor.blowJob(chunk, thisBlowfishKey)
            }

            thisDecryptedFile.appendBytes(decChunk)
            chunkCounter++
        }

        thisEncryptedFile.delete()

        if (sharedPreferences.getBoolean("tag_files", true)) {
            tagDownloadedSong(thisData, thisFilePathComplete)
        }

        completeDownload(thisData, thisFilePathComplete, downloadTaskId)
    }

    private fun tagDownloadedSong(thisData: TrackData, thisFilePathComplete: String) {
        val thisFile = File(thisFilePathComplete)
        val thisAudioFile = AudioFileIO.read(thisFile)
        val thisAudioTag = thisAudioFile.setNewDefaultTag()

        thisAudioTag.apply {
            setField(FieldKey.TITLE, thisData.primaryName ?: "")
            setField(FieldKey.ALBUM, thisData.secondaryName ?: "")
            setField(FieldKey.ALBUM_ARTIST, thisData.tertiaryName ?: "")
            setField(FieldKey.TRACK, thisData.tagData!!.trackNumber?.toString() ?: "")
            setField(FieldKey.BPM, thisData.tagData!!.trackBPM?.toString() ?: "")
            setField(FieldKey.GENRE, thisData.tagData!!.trackGenre ?: "")
            setField(FieldKey.YEAR, thisData.tagData!!.trackDate ?: "")
            setField(FieldKey.ARTIST, thisData.tagData!!.trackContributors ?: thisData.tertiaryName ?: "")
            setField(FieldKey.LYRICS, thisData.tagData!!.trackLyrics ?: "")
        }

        if (!thisData.coverXL.isNullOrBlank()) {
            val preferredResolution = when (sharedPreferences.getInt("preferred_art_resolution", 3)) {
                0 -> "1800"
                1 -> "1400"
                2 -> "1200"
                3 -> "1000"
                4 -> "800"
                5 -> "500"
                6 -> "250"
                else -> "1000"
            }

            val artworkSubExtension = if (sharedPreferences.getBoolean("get_png_artwork", false)) {
                ".png"
            } else {
                ".jpg"
            }

            var thisTempFile = File("${cacheDir.absolutePath}/${thisData.linkedAlbumId}_$preferredResolution$artworkSubExtension.tmp")

            if (!thisTempFile.exists()) {
                val artDataPair = downloadArtwork(thisData.coverXL, preferredResolution, artworkSubExtension)

                if (artDataPair.first != null) {
                    thisTempFile = File("${cacheDir.absolutePath}/${thisData.linkedAlbumId}_${artDataPair.second}$artworkSubExtension.tmp")

                    if (!thisTempFile.exists()) {
                        thisTempFile.createNewFile()
                        thisTempFile.writeBytes(artDataPair.first as ByteArray)
                    }
                }
            }

            if (thisTempFile.exists()) {
                val thisArtwork = ArtworkFactory.createArtworkFromFile(thisTempFile)
                thisAudioTag.addArtwork(thisArtwork)
            }
        }

        thisAudioFile.save()
    }

    private fun completeDownload(thisData: TrackData, thisFilePathComplete: String, downloadTaskId: String) {
        sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(File(thisFilePathComplete))))

        downloadEndNotification(
            downloadTaskId,
            thisData.primaryName!!,
            thisData.tertiaryName!!,
            getString(R.string.download_complete)
        )

        requestSnackbar(getString(R.string.successfully_downloaded, thisData.primaryName))
        removeTask(downloadTaskId)
        killIfNeeded()
    }

    private fun downloadArtwork(artUrl: String? = null, preferredResolution: String = "1000", artworkExtension: String): Pair<ByteArray?, String?> {
        return try {
            Pair(URL(artUrl!!.replace("1000x1000", "${preferredResolution}x$preferredResolution").replace(".jpg", artworkExtension)).readBytes(), preferredResolution)
        } catch (e: Exception) {
            if (preferredResolution == "1000") {
                Pair(null, null)
            } else {
                Log.w(TAG, "Failed to fetch artwork at $preferredResolution, falling back to 1000")
                Pair(downloadArtwork(artUrl!!, "1000", artworkExtension).first, "1000")
            }
        }
    }

    private fun requestSnackbar(snackbarText: String) {
        try {
            activityMessenger?.send(Message().apply {
                obj = "request_snackbar"
                data.putString("snackbarText", snackbarText)
            })
        } catch (e: Exception) {
            Log.w(TAG, e.message!!)
        }
    }

    private var notifId = 100
    private var notifs: ArrayMap<String, ArrayMap<Int, NotificationCompat.Builder>> = ArrayMap()

    fun newDownloadNotification(downloadTaskId: String, name: String, artist: String) {
        val thisNotifId = notifId++

        val cancelIntent = Intent(this, CancelReceiver::class.java)
        cancelIntent.putExtra("taskId", downloadTaskId)
        cancelIntent.action = "com.nick80835.add.CANCEL_DOWNLOAD"

        val pendingCancelIntent = PendingIntent.getBroadcast(this, thisNotifId, cancelIntent, PendingIntent.FLAG_ONE_SHOT)

        val thisBuilder = NotificationCompat.Builder(this, downloadChannelID).apply {
            setSmallIcon(android.R.drawable.stat_sys_download)
            setContentTitle("$name - $artist")
            setProgress(0, 0, true)
            setOnlyAlertOnce(true)
            setTimeoutAfter(6000000)
            setOngoing(true)
            addAction(0, getString(R.string.cancel), pendingCancelIntent)
        }

        val thisBuilderMap: ArrayMap<Int, NotificationCompat.Builder> = ArrayMap()

        thisBuilderMap[thisNotifId] = thisBuilder

        notifs[downloadTaskId] = thisBuilderMap

        NotificationManagerCompat.from(this).notify(
            downloadTaskId,
            notifs.getValue(downloadTaskId).keyAt(0),
            thisBuilder.build()
        )
    }

    fun updateDownloadProgress(downloadTaskId: String, progress: Int, progressSize: String) {
        val thisBuilder = notifs.getValue(downloadTaskId).getValue(notifs.getValue(downloadTaskId).keyAt(0))

        thisBuilder.apply {
            setProgress(100, progress, false)
            setContentText(progressSize)
        }

        NotificationManagerCompat.from(this).notify(
            downloadTaskId,
            notifs.getValue(downloadTaskId).keyAt(0),
            thisBuilder.build()
        )
    }

    fun downloadEndNotification(downloadTaskId: String, name: String, artist: String, reason: String) {
        NotificationManagerCompat.from(this).cancel(
            downloadTaskId,
            notifs.getValue(downloadTaskId).keyAt(0)
        )

        val thisBuilder = NotificationCompat.Builder(this, downloadChannelID).apply {
            setSmallIcon(android.R.drawable.stat_sys_download_done)
            setContentTitle("$name - $artist")
            setContentText(reason)
            setOnlyAlertOnce(true)
            setTimeoutAfter(6000000)
        }

        NotificationManagerCompat.from(this).notify(
            downloadTaskId,
            notifs.getValue(downloadTaskId).keyAt(0),
            thisBuilder.build()
        )

        notifs.remove(downloadTaskId)
    }

    fun downloadCancelNotification(downloadTaskId: String) {
        NotificationManagerCompat.from(this).cancel(
            downloadTaskId,
            notifs.getValue(downloadTaskId).keyAt(0)
        )

        notifs.remove(downloadTaskId)
    }

    fun cancelDownload(downloadTaskId: String) {
        if (downloadTaskId in downloadTasks) {
            val thisTask = downloadTasks.getValue(downloadTaskId)
            thisTask.cancelDownload()
        }
    }

    private fun String.scrub(): String {
        var thisScrubbedString = this.replace("/", "_")
        thisScrubbedString = thisScrubbedString.replace(".", "")

        if (thisScrubbedString == "") {
            thisScrubbedString = "_"
        }

        return thisScrubbedString
    }
}

class CancelReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == "com.nick80835.add.CANCEL_DOWNLOAD") {
            val taskId = intent.extras!!.get("taskId") as String
            aidService?.cancelDownload(taskId)
        }
    }
}
