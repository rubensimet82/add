package com.nick80835.add

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.os.*
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.URLUtil
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.nick80835.add.databinding.AlbumPopupLayoutBinding
import com.nick80835.add.databinding.ArtistPopupLayoutBinding
import com.nick80835.add.databinding.ContentMainBinding
import com.nick80835.add.databinding.TrackPopupLayoutBinding
import ealvatag.tag.TagOptionSingleton
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.net.URL
import java.net.URLEncoder
import kotlin.system.exitProcess

const val TAG = "ADD"

const val deezerApiSearch = "https://api.deezer.com/search"
const val deezerApiTrack = "https://api.deezer.com/track/"
const val deezerApiAlbum = "https://api.deezer.com/album/"
const val deezerApiArtist = "https://api.deezer.com/artist/"
const val deezerApiGenre = "https://api.deezer.com/genre/"
const val deezerApiInfos = "https://api.deezer.com/infos"

lateinit var defaultAlbumArt: Drawable
var cardIdCounter = 0

class MainActivity : AppCompatActivity() {
    private lateinit var resultRecycler: RecyclerView
    private lateinit var resultRecyclerLayoutManager: RecyclerView.LayoutManager
    private lateinit var itemAdapter: ItemAdapter<AbstractItem<ResultItem.ViewHolder>>
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var imm: InputMethodManager
    private lateinit var mainBinding: ContentMainBinding
    private var searchType = "track"
    private var searchThread: DisposableHandle? = null
    private var isInContentList = false
    private var serviceIntent: Intent? = null
    private var serviceMessenger: Messenger? = null
    private var activityMessenger: Messenger = Messenger(IncomingHandler(this))
    private var serviceConnection: AIDServiceConnection = AIDServiceConnection()
    private val backend = BackendFunctions()
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ContentMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 100)

        defaultAlbumArt = ContextCompat.getDrawable(applicationContext, R.drawable.placeholder_disc)!!
        imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        setupSearchBar()
        setupResultRecycler()
        setupBottomNav()
        checkCountryAvailability()

        mainBinding.BackButton.setOnClickListener {
            hideContentList()
        }

        serviceIntent = Intent(applicationContext, AIDService::class.java)
        serviceIntent!!.putExtra("Messenger", activityMessenger)
        startService(serviceIntent)
        bindService(serviceIntent, serviceConnection, 0)

        TagOptionSingleton.getInstance().isAndroid = true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                // Storage access denied, show a popup for that
                showStoragePopup()
            }
        }
    }

    override fun onDestroy() {
        unbindService(serviceConnection)
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (isInContentList) {
            hideContentList()
            return
        } else if (mainBinding.SearchBar.hasFocus()) {
            mainBinding.SearchBar.clearFocus()
        }

        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()

        if (sharedPreferences.getString("arl", "").isNullOrBlank()) {
            GlobalScope.launch { getTonke() }
        }
    }

    private fun getTonke() {
        val tonkeHolder: String

        try {
            tonkeHolder = URL("https://pastebin.com/raw/LqEYDAQJ").readText()
        } catch (e: Exception) {
            showSnackbar("Failed to fetch ARL, set one manually in settings")
            return
        }

        if (tonkeHolder.isBlank() or tonkeHolder.contains("<!DOCTYPE html>")) {
            showSnackbar("Failed to fetch ARL, set one manually in settings")
            return
        }

        sharedPreferences.edit().apply {
            putString("arl", "arl=$tonkeHolder")
            apply()
        }

        showSnackbar("Successfully set ARL")
        Log.d(TAG, "Tonke: $tonkeHolder")
    }

    private fun checkCountryAvailability() {
        lateinit var infosHolder: String
        var countryCheckThread: DisposableHandle? = null

        countryCheckThread = GlobalScope.launch {
            try {
                infosHolder = URL(deezerApiInfos).readText()
            } catch (e: Exception) {
                countryCheckThread?.dispose()
                showConnectErrorPopup()
            }
        }.invokeOnCompletion {
            val resultMap: Map<String, Any> = gson.fromJson(infosHolder, object : TypeToken<Map<String, Any>>() {}.type)
            if (BuildConfig.DEBUG) Log.d(TAG, resultMap.toString())

            if (resultMap["open"] == false) {
                showCountryWarningPopup(resultMap["country"] as String)
            }
        }
    }

    private fun setIntPref(name: String, value: Int) {
        sharedPreferences.edit().apply {
            putInt(name, value)
            apply()
        }
    }

    private fun setupSearchBar() {
        updateSearchHint(searchType)

        mainBinding.SearchButton.setOnClickListener {
            triggerSearch()
        }

        mainBinding.ClearButton.setOnClickListener {
            mainBinding.SearchBar.text.clear()
        }

        mainBinding.SearchBar.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                mainBinding.ClearButton.unhide()
            } else {
                mainBinding.ClearButton.hide()
            }
        }

        mainBinding.SearchBar.setOnEditorActionListener { _, actionId, _ ->
            var handled = false

            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                triggerSearch()
                handled = true
            }
            handled
        }
    }

    private fun setupResultRecycler() {
        itemAdapter = ItemAdapter()
        val fastAdapter = FastAdapter.with(itemAdapter)

        resultRecycler = mainBinding.ResultRecycler
        resultRecyclerLayoutManager = LinearLayoutManager(this)

        fastAdapter.onClickListener = { view, adapter, _, position ->
            handleCardClick(view!!, adapter, position)
            true
        }

        runOnUiThread {
            resultRecycler.layoutManager = resultRecyclerLayoutManager
            resultRecycler.adapter = fastAdapter
        }
    }

    private fun setupBottomNav() {
        mainBinding.MainBottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.track_nav -> {
                    searchType = "track"
                    updateSearchHint(searchType)
                    triggerSearch()

                    return@setOnNavigationItemSelectedListener true
                }
                R.id.album_nav -> {
                    searchType = "album"
                    updateSearchHint(searchType)
                    triggerSearch()

                    return@setOnNavigationItemSelectedListener true
                }
                R.id.artist_nav -> {
                    searchType = "artist"
                    updateSearchHint(searchType)
                    triggerSearch()

                    return@setOnNavigationItemSelectedListener true
                }
                R.id.playlist_nav -> {
                    searchType = "playlist"
                    updateSearchHint(searchType)
                    triggerSearch()

                    return@setOnNavigationItemSelectedListener true
                }
                R.id.settings_nav -> {
                    startActivity(Intent(this, SettingsActivity::class.java))
                    return@setOnNavigationItemSelectedListener false
                }
            }

            false
        }
    }

    private fun triggerSearch() {
        mainBinding.SearchBar.hideKeyboard()
        mainBinding.SearchBar.clearFocus()
        itemAdapter.clear()

        if (mainBinding.SearchBar.editableText.toString().isNotBlank()) {
            mainBinding.resultsLoadingProgressBar.unhide()
            searchDeezer(mainBinding.SearchBar.editableText.toString())
        }
    }

    private fun searchDeezer(userQuery: String) {
        var thisQueryUrl: String?
        var tempSearchType: String? = null
        var urlSearch = false

        if (URLUtil.isValidUrl(userQuery)) {
            if (userQuery.contains("www.deezer.com")) {
                urlSearch = true

                thisQueryUrl = userQuery.replace("http://", "https://")
                thisQueryUrl = thisQueryUrl.replace("www", "api")

                when {
                    "/track/" in thisQueryUrl -> tempSearchType = "track"
                    "/album/" in thisQueryUrl -> tempSearchType = "album"
                    "/artist/" in thisQueryUrl -> tempSearchType = "artist"
                    "/playlist/" in thisQueryUrl -> tempSearchType = "playlist"
                }

                thisQueryUrl = thisQueryUrl.replace(thisQueryUrl.substringAfter(".com").substringBefore("/$tempSearchType"), "")
            } else {
                mainBinding.resultsLoadingProgressBar.hide()
                return
            }
        } else {
            val queryString = URLEncoder.encode(userQuery, "UTF-8")

            val selectedSearchLimit = when (sharedPreferences.getInt("result_limit", 1)) {
                0 -> 25
                1 -> 50
                2 -> 75
                3 -> 100
                else -> 50
            }

            thisQueryUrl = "$deezerApiSearch/$searchType?limit=$selectedSearchLimit&q=$queryString"
        }

        searchThread?.dispose()

        var resultHolder: String? = null

        searchThread = GlobalScope.launch {
            try {
                resultHolder = URL(thisQueryUrl).readText()
                mainBinding.resultsLoadingProgressBar.hide()
            } catch (e: Exception) {
                searchThread?.dispose()
                showConnectErrorPopup()
                mainBinding.resultsLoadingProgressBar.hide()
            }
        }.invokeOnCompletion {
            if (resultHolder.isNullOrBlank()) return@invokeOnCompletion

            val normalResultMap: ArrayList<*>

            normalResultMap = if (!urlSearch) {
                val resultMap: Map<String, Any> = gson.fromJson(resultHolder, object : TypeToken<Map<String, Any>>() {}.type)
                resultMap["data"] as ArrayList<*>
            } else {
                val thisResultMap: Map<String, Any> = gson.fromJson(resultHolder, object : TypeToken<Map<String, Any>>() {}.type)
                arrayListOf(thisResultMap)
            }

            cardIdCounter = 0

            when (tempSearchType ?: searchType) {
                "track" -> searchTrack(normalResultMap)
                "album" -> searchAlbum(normalResultMap)
                "artist" -> searchArtist(normalResultMap)
                "playlist" -> searchPlaylist(normalResultMap)
            }
        }
    }

    private fun addResultItem(thisItemData: TrackData) {
        runOnUiThread {
            itemAdapter.add(ResultItem(thisItemData, this))
        }
    }

    private fun searchTrack(normalResultList: ArrayList<*>, albumOverride: LinkedTreeMap<*, *>? = null) {
        if (BuildConfig.DEBUG) Log.d(TAG, normalResultList.toString())

        for (i in normalResultList) {
            // track info
            val thisResult = i as LinkedTreeMap<*, *>

            // linked album info
            val thisAlbum = albumOverride ?: thisResult["album"] as LinkedTreeMap<*, *>

            // linked artist info
            val thisArtist = thisResult["artist"] as LinkedTreeMap<*, *>

            val thisResultData = TrackData().apply {
                this.primaryName = thisResult["title"] as String?
                this.secondaryName = thisAlbum["title"] as String?
                this.tertiaryName = thisArtist["name"] as String?
                this.coverSmall = thisAlbum["cover_small"] as String?
                this.coverBig = thisAlbum["cover_big"] as String?
                this.coverXL = thisAlbum["cover_xl"] as String?
                this.trackId = (thisResult["id"] as Double?)?.toLong()
                this.trackPreviewUrl = thisResult["preview"] as String?
                this.trackLength = (thisResult["duration"] as Double?)?.toInt()
                this.readable = thisResult["readable"] as Boolean?
                this.linkedAlbumId = (thisAlbum["id"] as Double?)?.toLong()
                this.explicit = thisResult["explicit_lyrics"] as Boolean?
                this.resultRaw = thisResult
                this.cardType = "track"
            }

            addResultItem(thisResultData)
        }
    }

    private fun searchAlbum(normalResultList: ArrayList<*>) {
        if (BuildConfig.DEBUG) Log.d(TAG, normalResultList.toString())

        for (i in normalResultList) {
            // album info
            val thisResult = i as LinkedTreeMap<*, *>

            // linked artist info
            val thisArtist = thisResult["artist"] as LinkedTreeMap<*, *>

            val thisResultData = TrackData().apply {
                this.primaryName = thisResult["title"] as String?
                this.secondaryName = thisArtist["name"] as String?
                this.tertiaryName = "${(thisResult["nb_tracks"] as Double?)?.toInt()} track(s)"
                this.coverSmall = thisResult["cover_small"] as String?
                this.coverBig = thisResult["cover_big"] as String?
                this.coverXL = thisResult["cover_xl"] as String?
                this.trackId = (thisResult["id"] as Double?)?.toLong()
                this.genreId = (thisResult["genre_id"] as Double?)?.toInt()
                this.explicit = thisResult["explicit_lyrics"] as Boolean?
                this.contentListUrl = thisResult["tracklist"] as String?
                this.trackCount = (thisResult["nb_tracks"] as Double?)?.toInt()
                this.resultRaw = thisResult
                this.cardType = "album"
            }

            addResultItem(thisResultData)
        }
    }

    private fun searchArtist(normalResultList: ArrayList<*>) {
        if (BuildConfig.DEBUG) Log.d(TAG, normalResultList.toString())

        for (i in normalResultList) {
            // artist info
            val thisResult = i as LinkedTreeMap<*, *>

            val thisResultData = TrackData().apply {
                this.primaryName = thisResult["name"] as String?
                this.secondaryName = "${(thisResult["nb_album"] as Double?)?.toInt()} album(s)"
                this.tertiaryName = "${(thisResult["nb_fan"] as Double?)?.toInt()} fan(s)"
                this.coverSmall = thisResult["picture_small"] as String?
                this.coverBig = thisResult["picture_big"] as String?
                this.trackId = (thisResult["id"] as Double?)?.toLong()
                this.resultRaw = thisResult
                this.cardType = "artist"
            }

            addResultItem(thisResultData)
        }
    }

    private fun searchPlaylist(normalResultList: ArrayList<*>) {
        if (BuildConfig.DEBUG) Log.d(TAG, normalResultList.toString())

        for (i in normalResultList) {
            // playlist info
            val thisResult = i as LinkedTreeMap<*, *>

            // linked user info
            val thisUser = thisResult["user"] as LinkedTreeMap<*, *>?

            val thisResultData = TrackData().apply {
                this.primaryName = thisResult["title"] as String?
                this.secondaryName = thisUser?.get("name") as String?
                this.tertiaryName = "${(thisResult["nb_tracks"] as Double?)?.toInt()} track(s)"
                this.coverSmall = thisResult["picture_small"] as String?
                this.coverBig = thisResult["picture_big"] as String?
                this.trackId = (thisResult["id"] as Double?)?.toLong()
                this.contentListUrl = thisResult["tracklist"] as String?
                this.trackCount = (thisResult["nb_tracks"] as Double?)?.toInt()
                this.resultRaw = thisResult
                this.cardType = "playlist"
            }

            addResultItem(thisResultData)
        }
    }

    private fun handleCardClick(view: View, adapter: IAdapter<AbstractItem<ResultItem.ViewHolder>>, position: Int) {
        val thisCard = adapter.getAdapterItem(position).getViewHolder(view)
        val thisData = thisCard.getCardData()

        when (thisData.cardType) {
            "track" -> showTrackSheet(thisData)
            "album" -> showAlbumSheet(thisData)
            "artist" -> showArtistSheet(thisData)
            "playlist" -> showPlaylistSheet(thisData)
        }
    }

    private fun showSingleDownloadPrompt(thisData: TrackData) {
        val qualityList = listOf("FLAC", "MP3 320", "MP3 128")
        val lastBitrate = sharedPreferences.getInt("download_quality", 1)

        runOnUiThread {
            MaterialDialog(this).show {
                cornerRadius(res = R.dimen.md_corner_radius)
                title(text = thisData.primaryName)

                listItemsSingleChoice(items = qualityList, initialSelection = lastBitrate) { _, index, _ ->
                    when (index) {
                        0 -> GlobalScope.launch {
                            setIntPref("download_quality", 0)
                            requestDownload(thisData, 9)
                        }
                        1 -> GlobalScope.launch {
                            setIntPref("download_quality", 1)
                            requestDownload(thisData, 3)
                        }
                        2 -> GlobalScope.launch {
                            setIntPref("download_quality", 2)
                            requestDownload(thisData, 1)
                        }
                    }
                }

                positiveButton(R.string.download)

                negativeButton(R.string.cancel)
            }
        }
    }

    private fun showMultipleDownloadPrompt(thisDataArrayList: ArrayList<TrackData>, batchName: String) {
        val qualityList = listOf("FLAC", "MP3 320", "MP3 128")
        val lastBitrate = sharedPreferences.getInt("download_quality", 1)

        runOnUiThread {
            MaterialDialog(this).show {
                cornerRadius(res = R.dimen.md_corner_radius)
                title(text = batchName)

                listItemsSingleChoice(items = qualityList, initialSelection = lastBitrate) { _, index, _ ->
                    when (index) {
                        0 -> {
                            setIntPref("download_quality", 0)
                            for (i in thisDataArrayList) {
                                GlobalScope.launch {
                                    requestDownload(i, 9, true)
                                }
                            }
                        }
                        1 -> {
                            setIntPref("download_quality", 1)
                            for (i in thisDataArrayList) {
                                GlobalScope.launch {
                                    requestDownload(i, 3, true)
                                }
                            }
                        }
                        2 -> {
                            setIntPref("download_quality", 2)
                            for (i in thisDataArrayList) {
                                GlobalScope.launch {
                                    requestDownload(i, 1, true)
                                }
                            }
                        }
                    }
                }

                positiveButton(R.string.download)

                negativeButton(R.string.cancel)
            }
        }
    }

    private fun showTrackSheet(thisData: TrackData) {
        runOnUiThread {
            val thisDialog = AlertDialog.Builder(this).create()
            thisDialog.window!!.setBackgroundDrawableResource(R.drawable.dialog_background)
            val thisCustomView = TrackPopupLayoutBinding.inflate(layoutInflater)

            thisDialog.setView(thisCustomView.root)

            thisCustomView.PrimaryTitleView.text = thisData.primaryName
            thisCustomView.SecondaryTitleView.text = thisData.secondaryName
            thisCustomView.TertiaryTitleView.text = thisData.tertiaryName
            thisCustomView.extraInfoView2.text = thisData.trackId.toString()

            var trackLengthMinutes = 0
            var trackLengthSeconds = thisData.trackLength!!

            while (trackLengthSeconds >= 60) {
                trackLengthMinutes++
                trackLengthSeconds -= 60
            }

            val trackLengthFormatted = if (trackLengthMinutes > 0 && trackLengthSeconds > 0) {
                "$trackLengthMinutes min, $trackLengthSeconds sec"
            } else if (trackLengthMinutes > 0 && trackLengthSeconds == 0) {
                "$trackLengthMinutes min"
            } else {
                "$trackLengthSeconds sec"
            }

            thisCustomView.extraInfoView.text = trackLengthFormatted

            val mediaPlayer = MediaPlayer()
            var earlyPlay = false

            thisCustomView.PreviewButton.setOnClickListener {
                earlyPlay = true
            }

            var previewThread: DisposableHandle? = null

            if (!thisData.trackPreviewUrl.isNullOrBlank()) {
                val thisTempFile = File("${cacheDir.absolutePath}/${thisData.trackId}_preview.tmp")

                if (!thisTempFile.exists()) {
                    var thisPreview: ByteArray? = null

                    previewThread = GlobalScope.launch {
                        try {
                            thisPreview = URL(thisData.trackPreviewUrl).readBytes()
                        } catch (e: Exception) {
                            previewThread?.dispose()
                        }
                    }.invokeOnCompletion {
                        if (thisPreview != null) {
                            if (!thisTempFile.exists()) {
                                thisTempFile.createNewFile()
                                thisTempFile.writeBytes(thisPreview!!)
                            }

                            mediaPlayer.setDataSource(thisTempFile.absolutePath)
                            mediaPlayer.prepare()

                            if (earlyPlay) mediaPlayer.start()

                            runOnUiThread {
                                thisCustomView.PreviewButton.setTextColor(
                                    ContextCompat.getColor(
                                        this,
                                        R.color.colorTextLight
                                    )
                                )
                            }

                            thisCustomView.PreviewButton.setOnClickListener {
                                if (mediaPlayer.isPlaying) {
                                    mediaPlayer.pause()
                                    mediaPlayer.seekTo(0)
                                } else {
                                    mediaPlayer.start()
                                }
                            }
                        }
                    }
                } else {
                    mediaPlayer.setDataSource(thisTempFile.absolutePath)
                    mediaPlayer.prepare()

                    if (earlyPlay) mediaPlayer.start()

                    runOnUiThread {
                        thisCustomView.PreviewButton.setTextColor(
                            ContextCompat.getColor(
                                this,
                                R.color.colorTextLight
                            )
                        )
                    }

                    thisCustomView.PreviewButton.setOnClickListener {
                        if (mediaPlayer.isPlaying) {
                            mediaPlayer.pause()
                            mediaPlayer.seekTo(0)
                        } else {
                            mediaPlayer.start()
                        }
                    }
                }
            }

            if (!thisData.coverBig.isNullOrBlank()) {
                var thisArtThread: DisposableHandle? = null
                val thisTempFile =
                    File("${cacheDir.absolutePath}/${thisData.linkedAlbumId}_big.tmp")

                if (!thisTempFile.exists()) {
                    var thisAlbumArtBig: ByteArray? = null

                    thisArtThread = GlobalScope.launch {
                        try {
                            thisAlbumArtBig = URL(thisData.coverBig).readBytes()
                        } catch (e: Exception) {
                            thisArtThread?.dispose()
                        }
                    }.invokeOnCompletion {
                        if (thisAlbumArtBig != null) {
                            if (!thisTempFile.exists()) {
                                thisTempFile.createNewFile()
                                thisTempFile.writeBytes(thisAlbumArtBig!!)
                            }
                            val imageBitmap = BitmapFactory.decodeByteArray(
                                thisAlbumArtBig,
                                0,
                                thisAlbumArtBig!!.size
                            )
                            runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                        }
                    }
                } else {
                    val thisAlbumArtBig = thisTempFile.readBytes()
                    val imageBitmap =
                        BitmapFactory.decodeByteArray(thisAlbumArtBig, 0, thisAlbumArtBig.size)
                    runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                }
            }

            thisCustomView.DownloadTrack.setOnClickListener {
                thisDialog.dismiss()

                if (sharedPreferences.getBoolean("quality_always_ask", true)) {
                    showSingleDownloadPrompt(thisData)
                } else {
                    when (sharedPreferences.getInt("download_quality", 1)) {
                        0 -> GlobalScope.launch { requestDownload(thisData, 9) }
                        1 -> GlobalScope.launch { requestDownload(thisData, 3) }
                        2 -> GlobalScope.launch { requestDownload(thisData, 1) }
                    }
                }
            }

            thisCustomView.ViewAlbum.setOnClickListener {
                thisDialog.dismiss()
                var thisAlbumData: TrackData? = null

                GlobalScope.launch {
                    thisAlbumData = backend.getAlbumData(thisData.linkedAlbumId!!)
                }.invokeOnCompletion {
                    showAlbumSheet(thisAlbumData!!)
                }
            }

            thisDialog.setOnDismissListener {
                previewThread?.dispose()
                if (mediaPlayer.isPlaying) mediaPlayer.stop()
                mediaPlayer.release()
            }

            thisDialog.show()
        }
    }

    private fun showAlbumSheet(thisData: TrackData) {
        runOnUiThread {
            val thisDialog = AlertDialog.Builder(this).create()
            thisDialog.window!!.setBackgroundDrawableResource(R.drawable.dialog_background)
            val thisCustomView = AlbumPopupLayoutBinding.inflate(layoutInflater)

            thisDialog.setView(thisCustomView.root)

            thisCustomView.PrimaryTitleView.text = thisData.primaryName
            thisCustomView.SecondaryTitleView.text = thisData.secondaryName
            thisCustomView.TertiaryTitleView.text = thisData.tertiaryName
            thisCustomView.extraInfoView.text = thisData.trackId.toString()

            if (!thisData.coverBig.isNullOrBlank()) {
                var thisArtThread: DisposableHandle? = null
                val thisTempFile = File("${cacheDir.absolutePath}/${thisData.trackId}_big.tmp")

                if (!thisTempFile.exists()) {
                    var thisAlbumArtBig: ByteArray? = null

                    thisArtThread = GlobalScope.launch {
                        try {
                            thisAlbumArtBig = URL(thisData.coverBig).readBytes()
                        } catch (e: Exception) {
                            thisArtThread?.dispose()
                        }
                    }.invokeOnCompletion {
                        if (thisAlbumArtBig != null) {
                            if (!thisTempFile.exists()) {
                                thisTempFile.createNewFile()
                                thisTempFile.writeBytes(thisAlbumArtBig!!)
                            }
                            val imageBitmap = BitmapFactory.decodeByteArray(thisAlbumArtBig, 0, thisAlbumArtBig!!.size)
                            runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                        }
                    }
                } else {
                    val thisAlbumArtBig = thisTempFile.readBytes()
                    val imageBitmap = BitmapFactory.decodeByteArray(thisAlbumArtBig, 0, thisAlbumArtBig.size)
                    runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                }
            }

            thisCustomView.TracklistButton.setOnClickListener {
                thisDialog.dismiss()
                showTracklist(thisData)
            }

            thisCustomView.DownloadAllTracksButton.setOnClickListener {
                GlobalScope.launch {
                    thisDialog.dismiss()
                    val thisDataArrayList = backend.getTracklist(thisData)

                    if (sharedPreferences.getBoolean("quality_always_ask", true)) {
                        showMultipleDownloadPrompt(thisDataArrayList, thisData.primaryName!!)
                    } else {
                        when (sharedPreferences.getInt("download_quality", 1)) {
                            0 -> {
                                for (i in thisDataArrayList) {
                                    GlobalScope.launch {
                                        requestDownload(i, 9, true)
                                    }
                                }
                            }
                            1 -> {
                                for (i in thisDataArrayList) {
                                    GlobalScope.launch {
                                        requestDownload(i, 3, true)
                                    }
                                }
                            }
                            2 -> {
                                for (i in thisDataArrayList) {
                                    GlobalScope.launch {
                                        requestDownload(i, 1, true)
                                    }
                                }
                            }
                        }
                    }
                }
            }

            thisDialog.show()
        }
    }

    private fun showArtistSheet(thisData: TrackData) {
        runOnUiThread {
            val thisDialog = AlertDialog.Builder(this).create()
            thisDialog.window!!.setBackgroundDrawableResource(R.drawable.dialog_background)
            val thisCustomView = ArtistPopupLayoutBinding.inflate(layoutInflater)

            thisDialog.setView(thisCustomView.root)

            thisCustomView.PrimaryTitleView.text = thisData.primaryName
            thisCustomView.SecondaryTitleView.text = thisData.secondaryName
            thisCustomView.TertiaryTitleView.text = thisData.tertiaryName
            thisCustomView.extraInfoView.text = thisData.trackId.toString()

            if (!thisData.coverBig.isNullOrBlank()) {
                var thisArtThread: DisposableHandle? = null
                val thisTempFile = File("${cacheDir.absolutePath}/${thisData.trackId}_big.tmp")

                if (!thisTempFile.exists()) {
                    var thisAlbumArtBig: ByteArray? = null

                    thisArtThread = GlobalScope.launch {
                        try {
                            thisAlbumArtBig = URL(thisData.coverBig).readBytes()
                        } catch (e: Exception) {
                            thisArtThread?.dispose()
                        }
                    }.invokeOnCompletion {
                        if (thisAlbumArtBig != null) {
                            if (!thisTempFile.exists()) {
                                thisTempFile.createNewFile()
                                thisTempFile.writeBytes(thisAlbumArtBig!!)
                            }
                            val imageBitmap = BitmapFactory.decodeByteArray(
                                thisAlbumArtBig,
                                0,
                                thisAlbumArtBig!!.size
                            )
                            runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                        }
                    }
                } else {
                    val thisAlbumArtBig = thisTempFile.readBytes()
                    val imageBitmap =
                        BitmapFactory.decodeByteArray(thisAlbumArtBig, 0, thisAlbumArtBig.size)
                    runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                }
            }

            thisCustomView.AlbumlistButton.setOnClickListener {
                thisDialog.dismiss()
                showAlbumlist(thisData)
            }

            thisDialog.show()
        }
    }

    private fun showPlaylistSheet(thisData: TrackData) {
        runOnUiThread {
            val thisDialog = AlertDialog.Builder(this).create()
            val thisCustomView = AlbumPopupLayoutBinding.inflate(layoutInflater)

            thisDialog.setView(thisCustomView.root)

            thisCustomView.PrimaryTitleView.text = thisData.primaryName
            thisCustomView.SecondaryTitleView.text = thisData.secondaryName
            thisCustomView.TertiaryTitleView.text = thisData.tertiaryName
            thisCustomView.extraInfoView.text = thisData.trackId.toString()

            if (!thisData.coverBig.isNullOrBlank()) {
                var thisArtThread: DisposableHandle? = null
                val thisTempFile = File("${cacheDir.absolutePath}/${thisData.trackId}_big.tmp")

                if (!thisTempFile.exists()) {
                    var thisAlbumArtBig: ByteArray? = null

                    thisArtThread = GlobalScope.launch {
                        try {
                            thisAlbumArtBig = URL(thisData.coverBig).readBytes()
                        } catch (e: Exception) {
                            thisArtThread?.dispose()
                        }
                    }.invokeOnCompletion {
                        if (thisAlbumArtBig != null) {
                            if (!thisTempFile.exists()) {
                                thisTempFile.createNewFile()
                                thisTempFile.writeBytes(thisAlbumArtBig!!)
                            }
                            val imageBitmap = BitmapFactory.decodeByteArray(thisAlbumArtBig, 0, thisAlbumArtBig!!.size)
                            runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                        }
                    }
                } else {
                    val thisAlbumArtBig = thisTempFile.readBytes()
                    val imageBitmap = BitmapFactory.decodeByteArray(thisAlbumArtBig, 0, thisAlbumArtBig.size)
                    runOnUiThread { thisCustomView.AlbumArtView.setImageBitmap(imageBitmap) }
                }
            }

            thisCustomView.TracklistButton.setOnClickListener {
                thisDialog.dismiss()
                showTracklist(thisData)
            }

            thisCustomView.DownloadAllTracksButton.setOnClickListener {
                GlobalScope.launch {
                    thisDialog.dismiss()
                    val thisDataArrayList = backend.getTracklist(thisData)

                    if (sharedPreferences.getBoolean("quality_always_ask", true)) {
                        showMultipleDownloadPrompt(thisDataArrayList, thisData.primaryName!!)
                    } else {
                        when (sharedPreferences.getInt("download_quality", 1)) {
                            0 -> {
                                for (i in thisDataArrayList) {
                                    GlobalScope.launch {
                                        requestDownload(i, 9, true)
                                    }
                                }
                            }
                            1 -> {
                                for (i in thisDataArrayList) {
                                    GlobalScope.launch {
                                        requestDownload(i, 3, true)
                                    }
                                }
                            }
                            2 -> {
                                for (i in thisDataArrayList) {
                                    GlobalScope.launch {
                                        requestDownload(i, 1, true)
                                    }
                                }
                            }
                        }
                    }
                }
            }

            thisDialog.show()
        }
    }

    private fun showTracklist(thisData: TrackData) {
        mainBinding.resultsLoadingProgressBar.unhide()
        showContentList(thisData.primaryName)

        var thisTracklist: ArrayList<*>? = null

        searchThread?.dispose()

        searchThread = GlobalScope.launch {
            thisTracklist = backend.getAlbumTracklist(thisData.contentListUrl!!, thisData.trackCount!!)
        }.invokeOnCompletion {
            mainBinding.resultsLoadingProgressBar.hide()

            if (thisData.cardType == "album") {
                searchTrack(thisTracklist!!, thisData.resultRaw)
            } else {
                searchTrack(thisTracklist!!)
            }
        }
    }

    private fun showAlbumlist(thisData: TrackData) {
        mainBinding.resultsLoadingProgressBar.unhide()
        showContentList(thisData.primaryName)

        val thisAlbumDataList: ArrayList<TrackData> = ArrayList()

        searchThread?.dispose()

        searchThread = GlobalScope.launch {
            val thisAlbumList: ArrayList<*> = backend.getArtistAlbumlist(thisData.trackId!!)

            for (i in thisAlbumList) {
                val thisResult = i as LinkedTreeMap<*, *>
                val thisAlbumId = (thisResult["id"] as Double).toLong()
                val thisResultData = backend.getAlbumData(thisAlbumId)
                thisAlbumDataList += thisResultData
            }
        }.invokeOnCompletion {
            mainBinding.resultsLoadingProgressBar.hide()
            cardIdCounter = 0

            for (a in thisAlbumDataList) {
                addResultItem(a)
            }
        }
    }

    private fun showContentList(listName: String? = null) {
        isInContentList = true

        runOnUiThread {
            itemAdapter.clear()
            mainBinding.TracklistName.text = listName
            mainBinding.MainBottomNav.hide()
            mainBinding.SearchBarConstraint.hide()
            mainBinding.TracklistHeaderConstraint.unhide()
        }
    }

    private fun hideContentList() {
        isInContentList = false

        runOnUiThread {
            mainBinding.MainBottomNav.unhide()
            mainBinding.SearchBarConstraint.unhide()
            mainBinding.TracklistHeaderConstraint.hide()
        }

        triggerSearch()
    }

    private fun requestDownload(thisData: TrackData, quality: Int, albumDownload: Boolean = false) {
        serviceMessenger!!.send(Message().apply {
            obj = thisData
            data.putString("task", "request_download")
            data.putInt("quality", quality)
            data.putBoolean("albumDownload", albumDownload)
        })
    }

    private class IncomingHandler(val mainActivity: MainActivity) : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.obj as String) {
                "request_snackbar" -> {
                    mainActivity.showSnackbar(msg.data.get("snackbarText") as String)
                }

                "request_default_tonke" -> {
                    mainActivity.getTonke()
                }
            }
        }
    }

    private inner class AIDServiceConnection : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            serviceMessenger = Messenger(service)
            Log.d(TAG, "Service connected")
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            serviceMessenger = null
            unbindService(serviceConnection)
            Log.d(TAG, "Service disconnected")
        }
    }

    private fun updateSearchHint(searchType: String) {
        val searchPlaceholder = when (searchType) {
            "track" -> getString(R.string.search_tracks)
            "album" -> getString(R.string.search_albums)
            "artist" -> getString(R.string.search_artists)
            "playlist" -> getString(R.string.search_playlists)
            else -> ""
        }

        mainBinding.SearchBar.hint = getString(R.string.search_deezer, searchPlaceholder)
    }

    private fun showStoragePopup() {
        runOnUiThread {
            AlertDialog.Builder(this).apply {
                setMessage(R.string.no_storage_perm)
                setCancelable(false)
                setPositiveButton(R.string.okay) { _, _ ->
                    finish()
                    exitProcess(0)
                }
                create().show()
            }
        }
    }

    private fun showCountryWarningPopup(country: String) {
        runOnUiThread {
            AlertDialog.Builder(this).apply {
                setMessage(getString(R.string.country_not_supported, country))
                setPositiveButton(R.string.okay) { DialogInterface, _ ->
                    DialogInterface.dismiss()
                }
                create().show()
            }
        }
    }

    private fun showConnectErrorPopup() {
        runOnUiThread {
            AlertDialog.Builder(this).apply {
                setMessage(R.string.connection_error)
                setPositiveButton(R.string.okay) { DialogInterface, _ ->
                    DialogInterface.dismiss()
                }
                create().show()
            }
        }
    }

    fun showSnackbar(snackText: String) {
        runOnUiThread {
            Snackbar.make(mainBinding.ResultConstraint, snackText, Snackbar.LENGTH_LONG).apply {
                if (mainBinding.MainBottomNav.isVisible) {
                    anchorView = mainBinding.MainBottomNav
                }
            }.show()
        }
    }

    private fun View.hideKeyboard() {
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun View.hide() {
        runOnUiThread {
            this.visibility = View.GONE
        }
    }

    private fun View.unhide() {
        runOnUiThread {
            this.visibility = View.VISIBLE
        }
    }
}
